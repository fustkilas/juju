---
---

JUJU is a Human Synthesiser made by [BRUD](http://BRUD.xyz). JUJU is built from modules called _Homo Modulus_ that are touch-sensitive, can be gesture-patched, are vocoders, and have an array of ins and outs. The music of the _Homo Modulus_ is called _Modular Folk_.

JUJU lives at: [Rib](http://ribrib.nl)    
Katendrechtse Lagedijk 490B  
3082 GJ Rotterdam— NL  
[http://ribrib.nl](http://ribrib.nl)

Next up:
JUJU III: _Moderato_, March 29, 2019   
Gerriet Krishna Sharma, *firniss*  
GITA  

JUJU TV: March 30, 2019  
Meng Qi  
Live from Beijing


### JUJU TV

{{< playlist PLBubmzgQRGLad0KJzqxi0n_iGWyk9wtwK >}}


#### UMMO, *Plaza Perifonica*

![](./images/sk.jpg)

We are facing a crisis. A crisis of direction.

Sound usually follows light. This is mere physics, for sound is slower. This can be used as a rubric to guesstimate an approaching storm. When you see a flash of lightning, start counting the time in seconds until you hear thunder. Divide this by three. This is how far the storm is, in kilometres. This order-of-appearance has reversed in recent times. Ambisonics is approaching fifty and experiencing a resurgence thanks to VR and 360 video. Sound, in this case, preceded vision.

The Danish semiotician Louis Hjelmslev defined the spatio-temporal size of the lexis as the “socialized unit of reading, of reception: in sculpture, the statue; in music, the ‘piece.’” The lexis of periphony has long languished in the doldrums of *Surround Sound* with its prescriptive vocabulary, military rhetoric, and emphasis on gimmickry. This has recently been expanded to the “binaural”, limited to singular experiences in headphones, and geared towards the Youtube-ASMR crowd. *Stereo* is Classical Greek for *solid*. *Periphony*, as coined by Michael Gerzon at the University of Oxford, is the notion of total immersive sound; or the *fluid image*. 

Periphony proposes new choreographies;
you are on longer bound to a rigid posture;
bigger isn't better, but more voices are
and “sweeter spots” are enlarged,
allowing for a greater collective experience.

As part of Radio JUJU, Brud is installing a *Sukuranburu Kousaten*, or a Scramble Crossing within Rib. Also known as a *Barnes Dance*, pedestrian scrambles “halt all vehicular traffic allowing pedestrians to cross an intersection in every direction at the same time”. Within this crossing Brud will place an immersive listening environment, the *Plaza Perifonica*. In keeping with the grassroots, DIY, peer-to-peer approach of Ham and amateur radio, Juju posits new forms of sonic experience: an autocephalous anomaly.

### Andrea Liu, *The Visitors*

![](./images/visitors.jpg)

The modular synthesiser anthropomorphised into a social body, with each parameter of signal (Frequency-Spectrum-Amplitude) processed differently. Visitor One wants to be integrated into the grand narrative of appreciating art. Visitor Two believes in the mystery and the unknowability and the ineffability of art. Visitor Three believes in the emancipatory potential of extreme experiences. Visitor Four yearns for linear narratives and emotional storytelling. Visitor Five believes art should be an intimate offering amongst friends–it should not have to be funnelled through the language and superstructure of the art world. Each Visitor is a speculation on a hypothetical audience member.

In any given performance, there is a vast reserve of untapped resources: the audience. I incorporate the hypothetical opinions, desires, and expectations of the audience as the content of my performance. The audience is a crystal, through which various desires, ideologies, assumptions, backgrounds, biases are refracted, projected, frustrated, appeased, challenged, or fulfilled.